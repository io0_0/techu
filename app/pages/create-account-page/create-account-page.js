import {
  CellsPage
} from '@cells/cells-page';
import {
  CellsI18nMixin as i18n
} from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import {
  html,
  css
} from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-core-generic-dp';
import '@bbva-web-components/bbva-currency-converter';
import '@bbva-web-components/bbva-core-select';


import {
  cleanUp
} from '../../elements/movements-dm/movements-dm';


class CreateAccountPage extends i18n(CellsPage) {
  static get is() {
    return 'create-account-page';
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._userInput = this.shadowRoot.querySelector('#user');
    this._passwordInput = this.shadowRoot.querySelector('#password');
    this._demoDp = this.shadowRoot.querySelector('#demodp');


  }

  render() {
    return html `

      <cells-template-paper-drawer-panel mode="seamed">

        <div slot="app__header">
          <bbva-header-main
          icon-left1="coronita:return-12"
            text="BBVA"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
        <bbva-form-field
          id="account_nuevo"
          auto-validate
          validate-on-blur
          label="numero de cuenta"}
          required>
        </bbva-form-field>
        <div class="input-field col s12">

        <select  label="etiqueta">
          <option value="USD">Dollares </option>
          <option value="PEN">Soles</option>
          <option value="EUR">Euros</option>

        </select >
        <label>Moneda</label>
          </div>

          <bbva-button-default
            @click=${this.handleValidation}>
              Crear Cuenta
          </bbva-button-default>
          <bbva-help-modal title-icon="coronita:info" title-text="${this.currentMessageTitle}" body-text="${this.currentMessage}" button-text="Accept" link-text="Link">


        </div>
     </cells-template-paper-drawer-panel>`;
  }
  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #000000;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
        justify-content: center;
      }
      #accountArea {
        display: flex;
        flex-direction: row;
      }
      bbva-card-select {
        font-size: 0.875em;
      }
      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      bbva-form-amount {
        --_field-input-color: #000000;
      }
      .container > * {
        margin-top: 10px;
      }
    `;
  }



  async handleValidation() {

        this.currentMessage =  "Cuenta creada con exito";
        this.currentMessageTitle = "Cuenta creada con exito";

        this.shadowRoot.querySelector('bbva-help-modal').open()

  }
}

window.customElements.define(CreateAccountPage.is, CreateAccountPage);
