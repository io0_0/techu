import {CellsPage} from "@cells/cells-page";
import {CellsI18nMixin as i18n} from "@cells-components/cells-i18n-mixin/cells-i18n-mixin.js";
import {html, css} from "lit-element";
import {spread} from "@open-wc/lit-helpers";

import "@bbva-web-components/bbva-list-movement";
import "@bbva-web-components/bbva-list-multistep-account";

import "@bbva-web-components/bbva-help-modal";

import {getAccounts} from "../../elements/accounts-dm/account-dm.js";

import {getMovements} from "../../elements/movements-dm/movements-dm.js";

import {normalizeUriString} from "../../elements/utils/text.js";

class DashboardPage extends i18n(CellsPage) {
  static get is() {
    return "dashboard-page";
  }

  constructor() {
    super();

    this.accounts = [];
    this.movements = {};
  }

  static get properties() {
    return {
      currentAcount: {
        type: String
      },
      userName: {
        type: String
      },
      userId: {
        type: String
      },
      accounts: {
        type: Array
      },
      movements: {
        type: Map
      }
    };
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._logoutModal = this.shadowRoot.querySelector("#logoutModal");
  }

  onPageEnter() {
    this.subscribe("user_name", userName => (this.userName = userName));
    this.subscribe("user_id", userId => (this.userId = userId));

    if (!this.accounts.length) {
      getAccounts(false,this.userId).then(accounts => {
        this.accounts = accounts;
      });
    }
  }

  onPageLeave() {}

  async createAccount({account, balance}) {
    this.navigate("create-account", {
      account
    });
  }
  async makeMovement({account, balance}) {
    this.publish('user_id', this.idUsuario);

    this.publish('accounts', this.accounts);

    this.navigate("create-movement");
  }

  async handleAccountClick({account, balance}) {
    this.publish("account_title", account);
    await this.accounts.forEach(element => {
      console.log(JSON.stringify(element));
      if (element.account != account){
      var pDemo = this.shadowRoot.querySelector(`#movC${element.account}`);
      var accountIten = this.shadowRoot.querySelector(
        `#account-iten${element.account}`
      );

      pDemo.style.display = "none";
      accountIten.style.backgroundColor = "white";}
    });

    var pDemo = this.shadowRoot.querySelector(`#movC${account}`);
    var accountIten = this.shadowRoot.querySelector(`#account-iten${account}`);

    if (pDemo.style.display === "none" ) {
      pDemo.style.display = "block";
      accountIten.style.backgroundColor = "#1973B8 ";
    } else {
      pDemo.style.display = "none";
      accountIten.style.backgroundColor = "white";
    }

    if (this.accounts.length) {

      await getMovements(false, account).then(movements => {
        this.movements[account] = movements;
        console.log('si llego a cargar los movimientos');
        console.log(this.movements);
      });

      //    this.navigate("movement-detail", {
      //      account
      //    });
    }
    this.currentAcount = account;

  }
  get accountList() {
    if (!this.accounts.length) {
      return null;
    }

    return this.accounts.map(account => {
      const accountProperties = this.buildAccountProperties(account);

      return html`
        <div class="account-row">
          <bbva-list-multistep-account ...="${spread(accountProperties)}">
          </bbva-list-multistep-account>
        </div>
        <div id="movC${accountProperties.number}" style="display: none;">
          ${this.accountMovements(accountProperties.number)}
          </div>
      `;
    });
  }
  accountMovements(accountId = "001105380275851274") {
    if (this.movements[accountId] == null) {
      console.log("tu acount es nulo papi" ,accountId);
      return null;
    } else {
      return this.movements[accountId].map(account => {
        const accountProperties = this.buildMovementProperties(account);
        return html`
          <bbva-list-movement ...="${spread(accountProperties)}">
          </bbva-list-movement>
          <div id="movC${accountProperties.number}"></div>
        `;
      });
    }
  }
  buildAccountProperties(accountParam) {
    const {account, currency_code, balance} = accountParam;

    return {
      name: "Cuenta de demostracion",
      amount: balance,
      number: account,
      "currency-code": currency_code,
      "local-currency": "PEN",
      details: "Gastos y comissiones: 0.75$",
      "@click": () => this.handleAccountClick(accountParam),
      id: `account-iten${account}`
    };
  }

  buildMovementProperties(movement) {
    const {
      account_origen,
      importe_origen,
      account_destino,
      importe_destino,
      tipo_cambio,
      fecha_mov
    } = movement;
    let importe_origen2 = importe_origen;
    if (account_destino != this.currentAcount) {
      importe_origen2 = importe_origen * -1;
    }
    console.log('please valve');
    return {
      "card-title": "Transferencia Efectivo",
      amount: importe_origen2,

      "local-currency": tipo_cambio,
      "currency-code": tipo_cambio,
      language: "es",
      description: "Transferencia Efectivo",
      mask: account_destino,
      icon: "bbva:receivemoneydollar",
      "@click": () => this.handleMovementClick(movement),
      class: "bbva-global-semidivider",
      "aria-label": "Ver detalle del pago",
      language: "es"
    };
  }

  render() {
    return html`
      <cells-template-paper-drawer-panel mode="seamed">
        <div slot="app__header">
          <bbva-header-main
            icon-left1="coronita:on"
            accessibility-text-icon-left1="Cerrar Sesión"
            @header-icon-left1-click=${() => this._logoutModal.open()}
            icon-right1="coronita:help"
            accessibility-text-icon-right1="Ayuda"
            @header-icon-right1-click=${() => this.navigate("help")}
            text=${this.t("dashboard-page.header") + this.userName}
          >
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <div class="cuentas1">
            ${this.accountList
              ? html`
                  ${this.accountList}
                `
              : html`<cells-skele  ton-loading-page visible></cells-skeleton-loading-page>`}
          </div>

          <div id="buttonArea" class="align-middle">
            <bbva-button-default @click=${this.createAccount}>
              ${this.t("create-account")}
            </bbva-button-default>

            <bbva-button-default @click=${this.makeMovement}>
              ${this.t("create-movement")}
            </bbva-button-default>
          </div>
          <bbva-help-modal
            id="logoutModal"
            header-icon="coronita:info"
            header-text=${this.t("dashboard-page.logout-modal.header")}
            button-text=${this.t("dashboard-page.logout-modal.button")}
            @help-modal-footer-button-click=${() => window.cells.logout()}
          >
            <div slot="slot-content">
              <span>${this.t("dashboard-page.logout-modal.slot")}</span>
            </div>
          </bbva-help-modal>
        </div>
      </cells-template-paper-drawer-panel>
    `;
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #002171;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
      }
      bbva-list-multistep-account {
        --bbva-list-multistep-number-color: #000000;
        --bbva-list-multistep-amount-color: #000000;
        --bbva-list-multistep-details-color: #000000;
        --bbva-list-multistep-name-color: #000000;
        --bbva-list-multistep-description-color: #000000;
        flex: 2000;
      }
      .cuentas1 {
        flex: 50px 50px 50px;
      }
      bbva-button-default {
        flex: 1;
      }
      #buttonArea {
        flex: 50px 50px 50px;

        justify-content: center;
      }
      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;
      }
      cells-template-paper-drawer-panel {
        background-color: #5472d3;
        justify-content: center;
      }
    `;
  }
}

window.customElements.define(DashboardPage.is, DashboardPage);
