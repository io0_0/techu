import {CellsPage} from "@cells/cells-page";
import {CellsI18nMixin as i18n} from "@cells-components/cells-i18n-mixin/cells-i18n-mixin.js";
import {html, css} from "lit-element";
import {spread} from "@open-wc/lit-helpers";

import "@bbva-web-components/bbva-form-field";
import "@bbva-web-components/bbva-form-password";
import "@bbva-web-components/bbva-button-default";
import "@bbva-web-components/bbva-core-generic-dp";

import "@bbva-web-components/bbva-composition-selector";
import "@bbva-web-components/bbva-help-modal";
import "@bbva-web-components/bbva-currency-converter";

import "../../elements/bbva-card-select/bbva-card-select";

import {getAccounts} from "../../elements/accounts-dm/account-dm.js";

import {
  getMovements,
  createMovements
} from "../../elements/movements-dm/movements-dm.js";
import "@bbva-web-components/bbva-composition-selector";

import "@bbva-web-components/bbva-form-amount";

import {cleanUp} from "../../elements/movements-dm/movements-dm";

class CreateMovementPage extends i18n(CellsPage) {
  static get is() {
    return "create-movement-page";
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._importe = this.shadowRoot.querySelector("#importe");
    this._destiniAccount = this.shadowRoot.querySelector("#account_destino");
    this._originAccount = this.shadowRoot.querySelector("#accountArea1");
    console.log(JSON.stringify(this._userInput));
    console.log(JSON.stringify(this._demoDp));

    const listItems = [
      {
        currencyType: "$",
        value: "23,00",
        conversion: "1",
        currencyId: "UYU",
        countryId: "uy",
        totalAmount: 23,
        conversionText: "Peso uruguayo"
      }
    ];
    const item = {
      currencyType: "R$",
      value: "1,00",
      conversion: "1",
      currencyId: "BRL",
      countryId: "br",
      totalAmount: 1,
      conversionText: "Real",
      disabledCountry: false
    };
  }
  constructor() {
    super();

    this.accounts = [];
    this.movements = {};
  }

  static get properties() {
    return {
      currentAcount: {
        type: String
      },
      userId: {
        type: String
      },
      currentAcountCurrency: {
        type: String
      },
      accounts: {
        type: Array
      },
      currentMessage: {
        type: String
      },
      currentMessageTitle: {
        type: String
      },
      userName: {
        type: String
      },
      accounts: {
        type: Array
      },
      movements: {
        type: Map
      }
    };
  }
  get accountList() {
    if (!this.accounts.length) {
      return null;
    }

    return this.accounts.map(account => {
      const accountProperties = this.buildAccountProperties(account);

      return html`
        <bbva-card-select ...="${spread(accountProperties)}">
        </bbva-card-select>
      `;
    });
  }
  buildAccountProperties(accountParam) {
    const {account, currency_code, balance} = accountParam;
    console.log("lectr", accountParam);

    return {
      "card-title":
        "Cuenta." + account.substring(account.length - 4, account.length),
      amount: balance,
      "currency-code": currency_code,
      value: `card${account}`,
      "@click": () => this.handleAccountClick(account, currency_code)
    };
  }

  handleAccountClick(account, currency) {
    this.currentAcount = account;
    this.currentAcountCurrency = currency;
  }
  render() {
    return html`

      <cells-template-paper-drawer-panel mode="seamed">

        <div slot="app__header">
          <bbva-header-main
          icon-left1="coronita:return-12"
            text="BBVA"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">


        <div id="accountArea" class="accountArea">
        <bbva-composition-selector id="accountArea1" accessibility-text="Example of legend" selected="${this
          .accounts.length - 1}">

          ${
            this.accountList
              ? html`
                  ${this.accountList}
                `
              : html`<cells-skele ton-loading-page visible></cells-skeleton-loading-page>`
          }
          </bbva-composition-selector>

        </div>


        <bbva-form-field
          id="account_origen"
          auto-validate
          validate-on-blur
          label=${this.t("origin-account")}
          value =${this.currentAcount}
          readonly=""
          required>
          value
        </bbva-form-field>

        <bbva-form-field
          id="account_destino"
          auto-validate
          validate-on-blur
          label=${this.t("destini-account")}
          required>
        </bbva-form-field>


        <bbva-form-amount
          id="importe"
          value = "asdasd"
          currency-code=${this.currentAcountCurrency}
          label=${this.t("import")}
          >
        </bbva-form-amount>
          <bbva-button-default
            @click=${this.makeMovement}>
              ${this.t("make-movement.button")}
          </bbva-button-default>

          <bbva-help-modal title-icon="coronita:info" title-text="${
            this.currentMessageTitle
          }" body-text="${
      this.currentMessage
    }" button-text="Accept" link-text="Link">

        </div>
     </cells-template-paper-drawer-panel>`;
  }

  async makeMovement() {
    let canContinue = true;

    var dat = {
      import: this._importe.value,
      currentAcount: this.currentAcount,
      destiniAcount: this._destiniAccount.value
    };

    console.log(JSON.stringify(dat));
    let rpta = await createMovements(
      false,
      this.currentAcount,
      this._destiniAccount.value,
      this._importe.amount
    );

    this.currentMessage = rpta.msg;
    this.currentMessageTitle = rpta.msg;

    this.shadowRoot.querySelector("bbva-help-modal").open();

    /*
    let status = 400;
    await fetch("http://localhost:3003/apitechu/v2/login", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      },

      body: JSON.stringify(dat) // body data type must match "Content-Type" header
    })
      .then(response => {
        status = response.status;
        console.log("status", status);
        return response.json();
      })
      .then(data => {
        console.log("Success Core:", data);
      })
      .catch(error => {
        console.error("Error:", error);
      });
    canContinue = canContinue && status === 200;
    if (canContinue) {
      this.publish("user_name", this._userInput.value);
      this.navigate("dashboard");
    }

    */
  }

   onPageEnter() {
    this.subscribe("user_id", userId => (this.userId = userId));
    this.subscribe("accounts", accounts => {this.accounts = accounts
      this.currentAcount = this.accounts[accounts.length - 1].account;
      this.currentAcountCurrency = this.accounts[accounts.length - 1].currency_code;


    });


  }

  onPageLeave() {
    // Cada vez que salgamos del login, limpiamos las cajas de texto.
  }

  static get styles() {
    return css`
      bbva-header-main {
        --bbva-header-main-bg-color: #000000;
      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
        justify-content: center;
      }
      #accountArea {
        display: flex;
        flex-direction: row;
      }
      bbva-card-select {
        font-size: 0.875em;
      }
      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }
      bbva-form-amount {
        --_field-input-color: #000000;
      }
      .container > * {
        margin-top: 10px;
      }
    `;
  }
}

window.customElements.define(CreateMovementPage.is, CreateMovementPage);
