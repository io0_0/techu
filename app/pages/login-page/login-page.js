import {
  CellsPage
} from '@cells/cells-page';
import {
  CellsI18nMixin as i18n
} from '@cells-components/cells-i18n-mixin/cells-i18n-mixin.js';
import {
  html,
  css
} from 'lit-element';

import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@bbva-web-components/bbva-button-default';
import '@bbva-web-components/bbva-core-generic-dp';

import {
  cleanUp
} from '../../elements/movements-dm/movements-dm';


class LoginPage extends i18n(CellsPage) {
  static get is() {
    return 'login-page';
  }

  firstUpdated(changedProps) {
    super.firstUpdated(changedProps);

    this._userInput = this.shadowRoot.querySelector('#user');
    this._passwordInput = this.shadowRoot.querySelector('#password');
    this._demoDp = this.shadowRoot.querySelector('#demodp');
    console.log(JSON.stringify(this._userInput));
    console.log(JSON.stringify(this._demoDp));

  }

    static get properties() {
      return {
        irUsuario: {
          type: String
        }

      };
    }
  render() {
    return html `

      <cells-template-paper-drawer-panel mode="seamed">

        <div slot="app__header">
          <bbva-header-main
          icon-left1="coronita:return-12"
            text="BBVA"">
          </bbva-header-main>
        </div>

        <div slot="app__main" class="container">
          <bbva-form-field
            id="user"
            auto-validate
            validate-on-blur
            label=${this.t('login-page.user-input')}
            required>
          </bbva-form-field>

          <bbva-core-generic-dp id="demodp"></bbva-core-generic-dp>

          <bbva-form-password
            id="password"
            auto-validate
            validate-on-blur
            label=${this.t('login-page.password-input')}
            required>
          </bbva-form-password>

          <bbva-button-default
            @click=${this.handleValidation}>
              ${this.t('login-page.button')}
          </bbva-button-default>


        </div>
     </cells-template-paper-drawer-panel>`;
  }


  async handleValidation() {
    let canContinue = true;

    [this._userInput, this._passwordInput].forEach((el) => (el.validate(), el.invalid && (canContinue = false)));


    var dat = {
      email: this._userInput.value,
      password: this._passwordInput.value
    };

    console.log(JSON.stringify(dat));
    const {host, pathLogin} = window.AppConfig;
    const endpoint = `${host}/${pathLogin}`;

    let status = 400;
    await fetch(endpoint, {
        method: 'POST', // or 'PUT'
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        },

        body: JSON.stringify(dat) // body data type must match "Content-Type" header
      })
      .then(response => {
        status = response.status;
        console.log('status', status);
        return response.json()
      })
      .then(data => {
        this.idUsuario =data.userid;
        console.log('Success Core:', data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
    canContinue = canContinue && status === 200;
    if (canContinue) {
      this.publish('user_name', this._userInput.value);
      this.publish('user_id', this.idUsuario);

      this.navigate('dashboard');
    }
  }

  onPageEnter() {
    // Cada vez que accedamos al login, simulamos una limpieza de los datos almacenados en memoria.
    cleanUp();
  }

  onPageLeave() {
    // Cada vez que salgamos del login, limpiamos las cajas de texto.
    setTimeout(() => [this._userInput, this._passwordInput].forEach((el) => el.clearInput()), 3 * 1000);
  }

  static get styles() {
    return css `
      bbva-header-main {
        --bbva-header-main-bg-color: #000000;

      }

      cells-template-paper-drawer-panel {
        background-color: #5472d3;
        justify-content: center;



      }

      .container {
        display: flex;
        flex-direction: column;
        align-items: center;
      }

      .container > * {
        margin-top: 10px;

      }
    `;
  }
}

window.customElements.define(LoginPage.is, LoginPage);
