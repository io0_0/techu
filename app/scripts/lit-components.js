// Import here your LitElement components (non critical for starting up)

// Auto generated imports below. DO NOT remove!
// will be replaced with imports
// ${filledByCellsWithAutoImports}

import '../pages/dashboard-page/dashboard-page.js';
import '../pages/dashboard-page/dashboard-movement-page.js';
import '../pages/dashboard-page/create-account-page.js';
import '../pages/dashboard-page/create-movement-page.js';

import '../pages/help-page/help-page.js';
import '../pages/movement-detail-page/movement-detail-page.js';
