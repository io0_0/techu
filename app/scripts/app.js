(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'dashboard': '/dashboard',
      'movement': '/movement',
      'create-movement': '/transferencia',
      'create-account': '/createAccount',

      'movement-detail': '/movement/:id/:label',
      'help': '/help'
    }
  });
}());
