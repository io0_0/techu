<!doctype html>

<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>practicioner-test</title>
  <!-- Place favicon.ico in the `app/` directory -->

  <!-- Chrome for Android theme color -->
  <meta name="theme-color" content="#303F9F">

  <!-- Web Application Manifest -->
  <link rel="manifest" href="manifest.json">

  <!-- Tile color for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="staticScaffold">
  <link rel="icon" sizes="192x192" href="resources/images/touch/chrome-touch-icon-192x192.png">
  <meta name="viewport" content="width-device-width, initial-scale=1">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="staticScaffold">
  <link rel="apple-touch-icon" href="resources/images/touch/apple-touch-icon.png">

  <!-- Tile icon for Win8 (144x144) -->
  <meta name="msapplication-TileImage" content="resources/images/touch/ms-touch-icon-144x144-precomposed.png">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <link rel="stylesheet" href="styles/main.css">
</head>

<body class="fullbleed layout vertical loading">
  <div id="splash"></div>
  <div id="app__content"></div>

  <!-- for a11y purposes -->
  <div id="announcer" aria-live="polite"></div>
  <script src="https://www.gstatic.com/dialogflow-console/fast/messenger/bootstrap.js?v=1"></script>
  <df-messenger
    chat-title="techu2"
    agent-id="780332fd-0081-40a0-8dee-f6e0034171ed"
    language-code="es"
  ></df-messenger>
  <script src="vendor/bowser.min.js"></script>
  <script type="module" src="scripts/app-module.js"></script>
</body>

</html>
