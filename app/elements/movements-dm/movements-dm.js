import {getAccounts,
getAccountsDetails,
getAccountsUserDetails,modifyAccountsDetails} from "../accounts-dm/account-dm.js";

let fetched = false;
let movements = [];
const TIPO_CAMBIO ={
    "USDPEN":3.61,
    "PENUSD":0.28,
    "USDEUR":0.85,
    "EURUSD":1.18,
    "EURPEN":4.26,
    "PENEUR":0.23,
    "PENPEN":1,
    "USDUSD":1,
    "EUREUR":1,


};
const shuffleArray = items => {
  // Shuffle array
  const shuffled = [...items].sort(() => 0.5 - Math.random());

  // Number of items to retrieve - at least, 2 of them.
  const length = Math.floor(Math.random() * items.length) + 2;

  // Get sub-array of first n elements after shuffled
  return shuffled.slice(0, length);
};

const buildMovementDetail = movement => {
  const {
    label,
    date,
    description,
    categoryDescription,
    parsedAmount,
    parsedAcountingBalance,
    badge
  } = movement;
  const movementDetail = [
    label && {
      key: "movement-detail.concept",
      value: label
    },
    date && {
      key: "movement-detail.date",
      value: date
    },
    description && {
      key: "movement-detail.description",
      value: description
    },
    categoryDescription && {
      key: "movement-detail.category",
      value: categoryDescription
    },
    parsedAmount && {
      key: "movement-detail.amount",
      value: `${parsedAmount.value} ${parsedAmount.currency}`
    },
    parsedAcountingBalance && {
      key: "movement-detail.currentBalance",
      value: `${parsedAcountingBalance.value} ${parsedAcountingBalance.currency}`
    },
    badge && {
      key: "movement-detail.badges",
      value: badge.label
    }
  ].filter(Boolean);

  return movementDetail;
};

export async function createMovements(
  force = false,
  accountOrigen = "001105380275851274",
  accountDestino = "001105380275851274",
  import2Transfert ="1"
) {


  const {host, pathTranfers, mock} = window.AppConfig;


  var dat = {
    account_origen: accountOrigen ,
    account_destino : accountDestino ,
    importe :  import2Transfert ,
    tipo_cambio : 1
  };
  let result = {};
  let account_origen_data =await  getAccountsDetails(false, accountOrigen)
  let account_destino_data =await  getAccountsDetails(false, accountDestino)
  if (account_destino_data.length!=1){
      console.log('error obteniendo cuenta destino');
      return {msg:"error obteniendo cuenta destino"};
  }

  console.log("account origien data ",JSON.stringify(account_origen_data));
  console.log("account destino data ",JSON.stringify(account_destino_data));
  console.log("cambio",TIPO_CAMBIO[account_origen_data[0].currency_code+account_destino_data[0].currency_code] );
  const endpoint = `${host}/${pathTranfers}`;
  let cambioMoneda = TIPO_CAMBIO[account_origen_data[0].currency_code+account_destino_data[0].currency_code] ;
  modifyAccountsDetails(false,accountOrigen,account_origen_data[0].balance-import2Transfert)
  modifyAccountsDetails(false,accountDestino,account_destino_data[0].balance+import2Transfert*cambioMoneda)


  await fetch(endpoint, {
    method: "POST", // or 'PUT'
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(dat)
  })
    .then(response => {
      var status = response.status;
      if (status == 204) {
        console.log("Success: NO DATA CAUSA ");

        result =  null;
      }
      return response.json();
    })
    .then(data => {
        result = data;
        console.log("Success:", data);

        return data;
      }
    )
    .catch(error => {
      console.error("Error: Causa arregla ps ",JSON.stringify(error));
    });
  let detaillUser = await getAccountsUserDetails(false, accountDestino)
  console.log('detail',detaillUser);
  //movements = shuffleArray(result);


  movements = result;
  return {msg: `Se realizo la operacion ha ${detaillUser[0].first_name} ${detaillUser[0].last_name}`};

}


export async function getMovements(
  force = false,
  account = "001105380275851274"
) {


  const {host, path, mock} = window.AppConfig;

  const endpoint = `${host}/${path}`;

  var dat = {
    account_origen: account
  };
  let result = {};

  await fetch(endpoint, {
    method: "POST", // or 'PUT'
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(dat)
  })
    .then(response => {
      var status = response.status;
      if (status == 204) {
        console.log("Success: NO DATA CAUSA ");

        result =  null;
      }
      return response.json();
    })
    .then(data => {
        result = data;
        console.log("Success:", data);

        return data;
      }
    )
    .catch(error => {
      console.error("Error: Causa arregla ps ",JSON.stringify(error));
    });

  //movements = shuffleArray(result);

  return result;
}

export async function getMovementDetail(id) {
  const movement = await getMovements().then(items =>
    items.find(item => item.id === id)
  );

  return buildMovementDetail(movement);
}

export function cleanUp() {
  fetched = false;
  movements = [];
}
