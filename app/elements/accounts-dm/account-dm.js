let fetched = false;
let accounts = [];

const shuffleArray = (items) => {
  // Shuffle array
  const shuffled = [...items].sort(() => 0.5 - Math.random());

  // Number of items to retrieve - at least, 2 of them.
  const length = Math.floor(Math.random() * items.length) + 2;

  // Get sub-array of first n elements after shuffled
  return shuffled.slice(0, length);
};

const buildaccountDetail = (account) => {
  const {
    label,
    date,
    description,
    categoryDescription,
    parsedAmount,
    parsedAcountingBalance,
    badge
  } = account;
  const accountDetail = [
    label && {
      key: 'account-detail.concept',
      value: label
    },
    date && {
      key: 'account-detail.date',
      value: date
    },
    description && {
      key: 'account-detail.description',
      value: description
    },
    categoryDescription && {
      key: 'account-detail.category',
      value: categoryDescription
    },
    parsedAmount && {
      key: 'account-detail.amount',
      value: `${parsedAmount.value} ${parsedAmount.currency}`
    },
    parsedAcountingBalance && {
      key: 'account-detail.currentBalance',
      value: `${parsedAcountingBalance.value} ${parsedAcountingBalance.currency}`
    },
    badge && {
      key: 'account-detail.badges',
      value: badge.label
    },
  ].filter(Boolean);

  return accountDetail;
};

export async function getAccounts(force = false, idUsuario = '21') {

  const {
    host,
    pathAccount,
    mock,
    pathUsers
  } = window.AppConfig;



  const endpoint = `${host}/${pathUsers}/${idUsuario}/${pathAccount}`;


  let accounts = {}

  await fetch(endpoint, {
      method: 'GET', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      return response.json();
    })
    .then(data => {
      accounts = data;
      console.log('Success:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });

  console.log(accounts);
  fetched = true;
  return accounts;
}

export async function getAccountsDetails(force = false, account = '21') {

  const {
    host,
    pathAccount,
    mock,
    pathUsers
  } = window.AppConfig;



  const endpoint = `${host}/${pathAccount}/${account}`;
  console.log('endpioint account',endpoint);

  let accounts = {}

  await fetch(endpoint, {
      method: 'GET', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      return response.json();
    })
    .then(data => {
      accounts = data;
      console.log('Success:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });

  console.log(accounts);
  fetched = true;
  return accounts;
}



export async function modifyAccountsDetails(force = false,  accountParam= "001104480260741382",
  balanceParam= 260.98,
) {

  const {
    host,
    pathAccount,
    mock,
    pathUsers
  } = window.AppConfig;



  const endpoint = `${host}/${pathAccount}/${accountParam}`;
  console.log('endpioint account',endpoint);


      var dat =  {
        account:accountParam,
        balance: balanceParam

};

  let status = 400;
  await fetch(endpoint, {
      method: 'PUT', // or 'PUT'
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      },

      body: JSON.stringify(dat) // body data type must match "Content-Type" header
    })
    .then(response => {
      status = response.status;
      console.log('status', status);
      return response.json()
    })
    .then(data => {
      console.log('Success Core:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });

  console.log(accounts);
  fetched = true;
  return accounts;
}
export async function getAccountsUserDetails(force = false, accountNumber = '21') {

  const {
    host,
    pathAccount,
    mock,
    pathUsers
  } = window.AppConfig;



  const endpoint = `${host}/${pathAccount}/${accountNumber}/user`;

  console.log('enpointFor user detail', endpoint);
  let accounts = {}

  await fetch(endpoint, {
      method: 'GET', // or 'PUT'
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      return response.json();
    })
    .then(data => {
      accounts = data;
      console.log('Success:', data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });

  console.log(accounts);
  fetched = true;
  return accounts;
}




export async function getaccountDetail(accountId) {
  const account = await getaccounts().then(items => items.find((item) => item.account === accountId));

  return buildaccountDetail(account);
}

export function cleanUp() {
  fetched = false;
  accounts = [];
}
