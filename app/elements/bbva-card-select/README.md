![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

# &lt;bbva-card-select&gt;

Selectable card with title and amount.

Example:

```html
<bbva-card-select
  amount="15.20"
  badge-text="Activado"
  badge-type="warning"
  currency-code="EUR"
  date="Vence 09/12/2018"
  text-amount="año"
  text-info="Incluido"
  card-title="Modalidad Básica"
>
</bbva-card-select>
```

## Icons

Since this component uses icons, it will need an [iconset](https://platform.bbva.com/en-us/developers/engines/cells/documentation/cells-architecture/composing-with-components/cells-icons) in your project as an application level dependency. In fact, this component uses an iconset in its demo.

## Styling

The following custom properties are available for styling:

### Custom properties

| Selector                       | CSS Property      | CSS Variable                         | Theme Variable          | Foundations/Fallback        |
| ------------------------------ | ----------------- | ------------------------------------ | ----------------------- | --------------------------- |
| :host([selected]) .amount      | color             | --bbva-white                         |                         | #ffffff                     |
| :host([selected]) .badge       | color             | --bbva-white                         |                         | #ffffff                     |
| :host([selected]) .title       | color             | --bbva-card-select-color-selected    | --bbva-white            | #ffffff                     |
| :host([selected]) .card-select | background-color  | --bbva-card-select-bg-color-selected | --bbva-dark-medium-blue | #1464a5                     |
| :host([selected]) .card-select | color             | --bbva-card-select-color-selected    | --bbva-white            | #ffffff                     |
| .date                          | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                      |
| .date                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                      |
| .text                          | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                      |
| .text                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                      |
| .text-amount                   | margin-left       | --bbva-global-spacer                 |                         | 0.5rem                      |
| .title                         | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                      |
| .title                         | margin-bottom     | --bbva-global-spacer                 |                         | 0.5rem                      |
| .title                         | line-height       | --bbva-global-spacer                 |                         | 0.5rem                      |
| .title                         | color             | --bbva-medium-blue                   |                         | #1973b8                     |
| .info                          | margin-left       | --bbva-global-spacer                 |                         | 0.5rem                      |
| .info                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                      |
| .icon                          | transition        | --bbva-fast-transition               |                         | 0.2s                        |
| .icon                          | --cells-icon-size | --bbva-card-select-icon-size         | --bbva-global-spacer    | 0.5rem                      |
| .icon                          | color             | --bbva-white                         |                         | #ffffff                     |
| .icon-off                      | top               | --bbva-global-spacer                 |                         | 0.5rem                      |
| .icon-off                      | left              | --bbva-global-spacer                 |                         | 0.5rem                      |
| .icon-off                      | width             | --bbva-global-spacer                 |                         | 0.5rem                      |
| .icon-off                      | height            | --bbva-global-spacer                 |                         | 0.5rem                      |
| .icon-off                      | background-color  | --bbva-card-select-bg-color-icon-off | --bbva-400              | #bdbdbd                     |
| .card-select                   | width             | --bbva-global-spacer                 |                         | 0.5rem                      |
| .card-select                   | padding           | --bbva-global-spacer                 |                         | 0.5rem                      |
| .card-select                   | background-color  | --bbva-card-select-label-bg-color    | --bbva-white            | #fff                        |
| .card-select                   | transition        | --bbva-fast-transition               |                         | 0.2s                        |
| .card-select                   | box-shadow        |                                      | --boxShadowType1        | foundations.boxShadow.type1 |

> Styling documentation generated by Cells CLI
