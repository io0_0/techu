import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import sinon from 'sinon';

import '../bbva-card-select.js';

suite('bbva-card-select', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(
      html`
        <bbva-card-select></bbva-card-select>
      `,
    );
    await el.updateComplete;
  });

  test('the card shows title', async () => {
    const expectedText = 'hello';
    const element = el.shadowRoot.querySelector('#title');
    el.cardTitle = expectedText;
    await el.updateComplete;
    assert.equal(element.innerText.trim(), expectedText);
  });

  test('the amount is showed with text', async () => {
    const expectedText = 'Hasta 130.35€';
    const element = el.shadowRoot.querySelector('#textAmount');
    el.textAmount = expectedText;
    await el.updateComplete;
    assert.equal(element.innerText.trim(), expectedText);
    assert.notOk(element.hidden);
  });

  test('the text info is shown with text', async () => {
    const expectedText = 'info';
    const element = el.shadowRoot.querySelector('#textInfo');
    el.textInfo = expectedText;
    await el.updateComplete;
    assert.equal(element.innerText.trim(), expectedText);
    assert.notOk(element.hidden);
  });

  test('the amount is hidden by default', () => {
    const element = el.shadowRoot.querySelector('#textAmount');
    assert.isOk(element.hidden);
  });

  test('the text is hidden by default', () => {
    const element = el.shadowRoot.querySelector('#text');
    assert.isOk(element.hidden);
  });

  test('the date is hidden by default', () => {
    const element = el.shadowRoot.querySelector('#date');
    assert.isOk(element.hidden);
  });

  test('the date is shown with text', async () => {
    const expectedText = '10/05/1991';
    const element = el.shadowRoot.querySelector('#date');
    el.date = expectedText;
    await el.updateComplete;
    assert.equal(element.innerText.trim(), expectedText);
    assert.notOk(element.hidden);
  });

  test('the card is an element focusable', async () => {
    el.focusable = true;
    await el.updateComplete;
    assert.equal(el.shadowRoot.querySelector('#card').tabIndex, '0');
  });

  test('the card is not an element focusable', async () => {
    el.focusable = false;
    await el.updateComplete;
    assert.equal(el.shadowRoot.querySelector('#card').tabIndex, '-1');
  });

  test('the card is a type radio', async () => {
    const $card = el.shadowRoot.querySelector('#card');
    el.type = 'radio';
    await el.updateComplete;
    assert.equal($card.getAttribute('role'), el.type);
  });

  test('the card is a type checkbox', async () => {
    const $card = el.shadowRoot.querySelector('#card');
    el.type = 'checkbox';
    await el.updateComplete;
    assert.equal($card.getAttribute('role'), el.type);
  });

  test('it shows with badge', async () => {
    el.badgeText = 'badge';
    await el.updateComplete;
    assert.notOk(el.shadowRoot.querySelector('#badgeDefault').hidden);
  });

  test('icon has correct size', async () => {
    const expectedSize = 10;
    const icon = el.shadowRoot.querySelector(`#icon`);
    el.iconCheckedSize = expectedSize;
    await el.updateComplete;
    assert.equal(icon.size, expectedSize);
  });

  test('it fires card-selected event on click', () => {
    const spy = sinon.spy();
    el.addEventListener('card-select-change', spy);
    el.shadowRoot.querySelector('#card').click();
    assert.isTrue(spy.calledOnce);
  });

  test('SPACEBAR should check the checkbox button', async () => {
    const event = new Event('keydown');
    event.keyCode = 32;
    const $card = el.shadowRoot.querySelector('#card');
    $card.dispatchEvent(event);
    await el.updateComplete;
    assert.equal($card.getAttribute('aria-checked'), 'true');
  });

  test('OTHER KEY shouldnt check the checkbox button', async () => {
    const event = new Event('keydown');
    event.keyCode = 33;
    const $card = el.shadowRoot.querySelector('#card');
    $card.dispatchEvent(event);
    await el.updateComplete;
    assert.equal($card.getAttribute('aria-checked'), 'false');
  });

  test('the card is focused itself', () => {
    const focusStub = sinon.stub(el.shadowRoot.querySelector('#card'), 'focus');
    el.focusElement();
    assert.isTrue(focusStub.calledOnce);
  });

  suite('Semantic Dom', () => {
    test('DOM - Structure test', () => {
      assert.dom.equalSnapshot(el);
    });

    test('SHADOW DOM - Structure test', () => {
      assert.shadowDom.equalSnapshot(el);
    });

    test('LIGHT DOM - Structure test', () => {
      assert.lightDom.equalSnapshot(el);
    });
  });
});
