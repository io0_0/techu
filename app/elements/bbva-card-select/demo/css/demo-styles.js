import { setDocumentCustomStyles } from '@cells-components/cells-lit-helpers';
import { css } from 'lit-element';

setDocumentCustomStyles(css`
  body {
    margin: 0;
    box-sizing: border-box;
    font-family: var(--cells-fontDefault, Benton Sans, sans-serif);
  }

  #iframeBody {
    margin: 8px;
    overflow: hidden;
    background-color: #f4f4f4;
  }

  @media (max-width: 768px) {
    *,
    *:before,
    *:after {
      -webkit-tap-highlight-color: transparent;
    }
  }
`);
