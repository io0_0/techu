# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.2.7](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.2.6...@bbva-web-components/bbva-card-select@3.2.7) (2020-10-16)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [3.2.6](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.2.0...@bbva-web-components/bbva-card-select@3.2.6) (2020-10-13)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [3.2.5](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.2.4...@bbva-web-components/bbva-card-select@3.2.5) (2020-10-07)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [3.2.4](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.2.0...@bbva-web-components/bbva-card-select@3.2.4) (2020-10-01)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [3.2.3](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.2.0...@bbva-web-components/bbva-card-select@3.2.3) (2020-09-28)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [3.2.2](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.2.0...@bbva-web-components/bbva-card-select@3.2.2) (2020-09-23)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [3.2.1](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.2.0...@bbva-web-components/bbva-card-select@3.2.1) (2020-09-14)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

# [3.2.0](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.0.0...@bbva-web-components/bbva-card-select@3.2.0) (2020-07-17)

### Features

- **bbva-card-select:** set outline styles and shadow ([0f90e5f](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/commits/0f90e5f818762c53a5ed80fe7f058a1d559e0e12))

# [3.1.0](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@3.0.0...@bbva-web-components/bbva-card-select@3.1.0) (2020-07-10)

### Features

- **bbva-card-select:** set outline styles and shadow ([0f90e5f](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/commits/0f90e5f818762c53a5ed80fe7f058a1d559e0e12))

# [3.0.0](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@2.0.0...@bbva-web-components/bbva-card-select@3.0.0) (2020-04-15)

### Code Refactoring

- change events names, payloads and demo listeners ([7b4a3f1](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/commits/7b4a3f1ccea8a072d2cfd6076324ce3ac2d9b2b4))

### Styles

- remove [@apply](http://globaldevtools.bbva.com:7999/apply) ([7739dea](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/commits/7739dea728bc8e90ac1d596b53911779e5fd1a04))

### BREAKING CHANGES

- removed all @apply tags and replaced dependent styles of this tag
- standarize events names and payloads

# [2.0.0](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/compare/@bbva-web-components/bbva-card-select@1.1.10...@bbva-web-components/bbva-card-select@2.0.0) (2020-03-26)

### Features

- constructable stylesheets and openwc ([d477312](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components-app/commits/d4773124f5a6ecda53e0e4d935bc988591f6c451))

## [1.1.10](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/compare/@bbva-web-components/bbva-card-select@1.1.9...@bbva-web-components/bbva-card-select@1.1.10) (2020-02-01)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [1.1.9](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/compare/@bbva-web-components/bbva-card-select@1.1.8...@bbva-web-components/bbva-card-select@1.1.9) (2019-12-17)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [1.1.8](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/compare/@bbva-web-components/bbva-card-select@1.1.7...@bbva-web-components/bbva-card-select@1.1.8) (2019-12-12)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [1.1.7](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/compare/@bbva-web-components/bbva-card-select@1.1.6...@bbva-web-components/bbva-card-select@1.1.7) (2019-11-15)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [1.1.6](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/compare/@bbva-web-components/bbva-card-select@1.1.5...@bbva-web-components/bbva-card-select@1.1.6) (2019-11-06)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [1.1.5](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/compare/@bbva-web-components/bbva-card-select@1.1.4...@bbva-web-components/bbva-card-select@1.1.5) (2019-11-04)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [1.1.4](http://globaldevtools.bbva.com:7999/cellscataloggovernance/bbva-web-components-fork/compare/@bbva-web-components/bbva-card-select@1.1.3...@bbva-web-components/bbva-card-select@1.1.4) (2019-10-18)

### Bug Fixes

- update lit helpers version ([01c876a](http://globaldevtools.bbva.com:7999/cellscataloggovernance/bbva-web-components-fork/commits/01c876aa84cfe98b6abbd2379127ad84df6a6cf4))

## [1.1.3](http://globaldevtools.bbva.com:7999/cellscataloggovernance/bbva-web-components-fork/compare/@bbva-web-components/bbva-card-select@1.1.2...@bbva-web-components/bbva-card-select@1.1.3) (2019-10-15)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

## [1.1.2](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/compare/@bbva-web-components/bbva-card-select@1.1.1...@bbva-web-components/bbva-card-select@1.1.2) (2019-10-14)

### Bug Fixes

- **bbva-card-select:** adapt to new amount ([f9417d2](http://globaldevtools.bbva.com:7999/bbva_global_ui_studio_web_components/bbva-web-components/commits/f9417d2))

## [1.1.1](http://globaldevtools.bbva.com:7999/cellscataloggovernance/bbva-web-components-fork/compare/@bbva-web-components/bbva-card-select@1.1.0...@bbva-web-components/bbva-card-select@1.1.1) (2019-10-08)

**Note:** Version bump only for package @bbva-web-components/bbva-card-select

# [1.1.0](http://globaldevtools.bbva.com:7999/cellscataloggovernance/bbva-web-components-fork/compare/@bbva-web-components/bbva-card-select@1.0.27...@bbva-web-components/bbva-card-select@1.1.0) (2019-09-19)

### Features

- add linting tools ([4394fa6](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/4394fa6))

## 1.0.27 (2019-09-19)

### Bug Fixes

- adapt bbva-button-list and bbva-clip-box to new cirio ([17229d1](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/17229d1))
- adapt bbva-card-select to new cirio ([de5b5c9](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/de5b5c9))
- fix mixins namespaces ([0043a35](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/0043a35))
- normalize demo file ([e335948](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/e335948))
- uupdate demo dependencies paths ([939d96c](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/939d96c))
- **bbva-card-select:** add stylemap directive to html, fix doc in js ([15be675](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/15be675))
- **html:** add crossorigin to all packages demos demo-build imports ([d29586c](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/d29586c))
- **js:** add missing extension points to bbva-list-link ([ec253fc](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/ec253fc))
- **json:** update cells icon dependencies ([3cd4087](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/3cd4087))

### Features

- update dependencies ([ca6e6c9](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/ca6e6c9))
- update dependencies to foundations styles and themes ([6cbc8aa](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/6cbc8aa))
- updated for demo ([7f8c21b](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/7f8c21b))
- **bbva-amount-mixin:** remove bbva-amount-mixin and usage in components ([14e2db8](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/14e2db8))
- **html, js, css:** add bbva-card-select to packages ([a9cd21e](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/a9cd21e))
- **json:** update lock dependencies ([56a46b5](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/56a46b5))
- add linting tools ([4394fa6](https://globaldevtools.bbva.com/bitbucket/projects/bbva_global_ui_studio_web_components/repos/bbva-web-components/commits/4394fa6))
