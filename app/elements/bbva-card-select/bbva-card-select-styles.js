import { css, unsafeCSS } from 'lit-element';
import * as foundations from '@bbva-web-components/bbva-foundations-styles';

export default css`:host {
  display: block;
  box-sizing: border-box; }

:host([hidden]),
[hidden] {
  display: none !important; }

*,
*:before,
*:after {
  box-sizing: inherit;
  margin: 0; }

.card-select {
  display: block;
  overflow: hidden;
  position: relative;
  font-family: inherit;
  width: calc(var(--bbva-global-spacer, 0.5rem) * 19);
  padding: calc(var(--bbva-global-spacer, 0.5rem) * 2);
  border-radius: 0.125rem;
  background-color: var(--bbva-card-select-label-bg-color, var(--bbva-white, #fff));
  transition: background-color var(--bbva-fast-transition, 0.2s) ease-in-out, color var(--bbva-fast-transition, 0.2s) ease-in-out, opacity var(--bbva-fast-transition, 0.2s) ease-in-out;
  user-select: none;
  outline: none;
  box-shadow: var(--boxShadowType1, ${unsafeCSS(foundations.boxShadow.type1)});
  margin: 0; }

.header {
  display: flex;
  align-items: center; }

.icon-off {
  content: ' ';
  position: absolute;
  top: calc(var(--bbva-global-spacer, 0.5rem) * 2);
  left: calc(var(--bbva-global-spacer, 0.5rem) * 2);
  width: calc(var(--bbva-global-spacer, 0.5rem) * 4);
  height: calc(var(--bbva-global-spacer, 0.5rem) * 4);
  border-radius: 50%;
  background-color: var(--bbva-card-select-bg-color-icon-off, var(--bbva-400, #bdbdbd)); }

.icon {
  opacity: 0;
  transition: opacity var(--bbva-fast-transition, 0.2s) ease-in-out;
  --cells-icon-size: var(--bbva-card-select-icon-size,calc(var(--bbva-global-spacer, 0.5rem) * 4));
  color: var(--bbva-white, #ffffff); }

.info {
  margin-left: var(--bbva-global-spacer, 0.5rem);
  font-size: 0.75rem;
  line-height: calc(var(--bbva-global-spacer, 0.5rem) * 3);
  font-style: italic; }

.title {
  margin-top: calc(var(--bbva-global-spacer, 0.5rem) * 2);
  margin-bottom: var(--bbva-global-spacer, 0.5rem);
  font-size: 1.125rem;
  line-height: calc(var(--bbva-global-spacer, 0.5rem) * 3);
  font-weight: 500;
  color: var(--bbva-medium-blue, #1973b8); }

.text-amount {
  display: inline-block;
  margin-left: calc(var(--bbva-global-spacer, 0.5rem) / 2);
  font-size: 0.9375rem; }

.text {
  margin-top: calc(var(--bbva-global-spacer, 0.5rem) / 2);
  font-size: 0.9375rem;
  line-height: calc(var(--bbva-global-spacer, 0.5rem) * 3); }

.date {
  margin-top: var(--bbva-global-spacer, 0.5rem);
  font-size: 0.875rem;
  line-height: calc(var(--bbva-global-spacer, 0.5rem) * 3);
  font-weight: 500;
  font-style: italic; }

.ellipsis {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis; }

:host([selected]) .card-select {
  background-color: var(--bbva-card-select-bg-color-selected, var(--bbva-dark-medium-blue, #1464a5));
  color: var(--bbva-card-select-color-selected, var(--bbva-white, #ffffff)); }

:host([selected]) .title {
  color: var(--bbva-card-select-color-selected, var(--bbva-white, #ffffff)); }

:host([selected]) .badge {
  color: var(--bbva-white, #ffffff); }

:host([selected]) .opacity-icon {
  opacity: 1; }

:host([selected]) .amount {
  color: var(--bbva-white, #ffffff); }

:host([selected]) .icon-off {
  opacity: 0; }
`;