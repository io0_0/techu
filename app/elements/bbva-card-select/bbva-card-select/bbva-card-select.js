import { LitElement, html } from 'lit-element';
import { ifDefined } from 'lit-html/directives/if-defined.js';
import { styleMap } from 'lit-html/directives/style-map.js';
import { getComponentSharedStyles } from '@cells-components/cells-lit-helpers';
import '@cells-components/cells-icon';
import '@bbva-web-components/bbva-amount';
import { CellsAmountMixin as cellsAmountMixin } from '@cells-components/cells-amount-mixin';
import '@bbva-web-components/bbva-badge-default';
import styles from './bbva-card-select-styles.js';

const KEY = { SPACE: 32 };
/**
Selectable card with title and amount.

Example:

```html
<bbva-card-select
  amount="15.20"
  badge-text="Activado"
  badge-type="warning"
  currency-code="EUR"
  date="Vence 09/12/2018"
  text-amount="año"
  text-info="Incluido"
  card-title="Modalidad Básica">
</bbva-card-select>
```

## Icons

Since this component uses icons, it will need an [iconset](https://platform.bbva.com/en-us/developers/engines/cells/documentation/cells-architecture/composing-with-components/cells-icons) in your project as an application level dependency. In fact, this component uses an iconset in its demo.

## Styling

The following custom properties are available for styling:

### Custom properties

| Selector                       | CSS Property      | CSS Variable                         | Theme Variable          | Foundations/Fallback                      |
| ------------------------------ | ----------------- | ------------------------------------ | ----------------------- | ----------------------------------------- |
| :host([selected]) .amount      | color             | --bbva-white                         |                         | #ffffff                                   |
| :host([selected]) .badge       | color             | --bbva-white                         |                         | #ffffff                                   |
| :host([selected]) .title       | color             | --bbva-card-select-color-selected    | --bbva-white            | #ffffff                                   |
| :host([selected]) .card-select | background-color  | --bbva-card-select-bg-color-selected | --bbva-dark-medium-blue | #1464a5                                   |
| :host([selected]) .card-select | color             | --bbva-card-select-color-selected    | --bbva-white            | #ffffff                                   |
| .date                          | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .date                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .text                          | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .text                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .text-amount                   | margin-left       | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .title                         | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .title                         | margin-bottom     | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .title                         | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .title                         | color             | --bbva-medium-blue                   |                         | #1973b8                                   |
| .info                          | margin-left       | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .info                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .icon                          | transition        | --bbva-fast-transition               |                         | 0.2s                                      |
| .icon                          | --cells-icon-size | --bbva-card-select-icon-size         | --bbva-global-spacer    | 0.5rem                                    |
| .icon                          | color             | --bbva-white                         |                         | #ffffff                                   |
| .icon-off                      | top               | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .icon-off                      | left              | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .icon-off                      | width             | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .icon-off                      | height            | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .icon-off                      | background-color  | --bbva-card-select-bg-color-icon-off | --bbva-400              | #bdbdbd                                   |
| .card-select                   | width             | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .card-select                   | padding           | --bbva-global-spacer                 |                         | 0.5rem                                    |
| .card-select                   | background-color  | --bbva-card-select-label-bg-color    | --bbva-white            | #fff                                      |
| .card-select                   | transition        | --bbva-fast-transition               |                         | 0.2s                                      |
| .card-select                   | box-shadow        |                                      | --boxShadowType1        | foundations.boxShadow.type1 |
> Styling documentation generated by Cells CLI

@customElement bbva-card-select
@polymer
@LitElement
@demo demo/index.html
@appliesMixin CellsAmountMixin
*/
class BbvaCardSelect extends cellsAmountMixin(LitElement) {
  static get is() {
    return 'bbva-card-select';
  }

  static get properties() {
    return {
      /**
       * Badge Text
       */
      badgeText: {
        type: String,
        attribute: 'badge-text',
      },
      /**
       * Badge Type
       */
      badgeType: {
        type: String,
        attribute: 'badge-type',
      },
      /**
       * Card Title
       */
      cardTitle: {
        type: String,
        attribute: 'card-title',
      },
      /**
       * Date text
       */
      date: {
        type: String,
      },
      /**
       * Text
       */
      text: {
        type: String,
      },
      /**
       * Amount text
       */
      textAmount: {
        type: String,
        attribute: 'text-amount',
      },
      /**
       * selected
       */
      selected: {
        type: Boolean,
        reflect: true,
      },
      /**
       * Information text
       */
      textInfo: {
        type: String,
        attribute: 'text-info',
      },
      /**
       * Icon
       */
      iconChecked: {
        type: String,
        attribute: 'icon-checked',
      },
      /**
       * type checkbox or radio
       */
      type: {
        type: String,
      },
      /**
       * Icon size
       */
      iconCheckedSize: {
        type: Number,
        attribute: 'icon-checked-size',
      },
      /**
       * element focusable or not
       */
      focusable: {
        type: Boolean,
      },
    };
  }

  constructor() {
    super();
    this.badgeText = '';
    this.badgeType = '';
    this.cardTitle = '';
    this.date = '';
    this.text = '';
    this.textAmount = '';
    this.selected = false;
    this.textInfo = '';
    this.iconChecked = 'coronita:correct';
    this.type = 'checkbox';
    this.focusable = true;
  }

  get _tabIndex() {
    return this.focusable ? '0' : '-1';
  }

  get _ariaSelected() {
    return this.selected ? String(this.selected) : 'false';
  }

  /**
   * Fired after selected change
   * @event card-select-change
   */
  updated(changedProps) {
    if (changedProps.has('selected')) {
      this.dispatchEvent(
        new CustomEvent('card-select-change', {
          detail: this.selected,
          bubbles: true,
          composed: true,
        }),
      );
    }
  }

  static get styles() {
    return [
      getComponentSharedStyles('bbva-global-dividers-shared-styles'),
      styles,
      getComponentSharedStyles('bbva-card-select-shared-styles'),
    ];
  }

  render() {
    const sizeStyle = this.iconCheckedSize ? `${this.iconCheckedSize}px` : '';
    return html`
      <div
        role="${this.type}"
        class="card-select"
        aria-checked="${this._ariaSelected}"
        @click="${this._changeSelected}"
        @keydown="${this._cardSelectedPress}"
        tabindex="${this._tabIndex}"
        id="card"
      >
        <div class="header">
          <span
            class="icon-off"
            style="${styleMap({ width: sizeStyle, height: sizeStyle })}"
          ></span>
          <cells-icon
            id="icon"
            icon="${this.iconChecked}"
            .size="${ifDefined(this.iconCheckedSize)}"
            class="icon opacity-icon"
          ></cells-icon>
          <span class="info" id="textInfo">${this.textInfo}</span>
        </div>
        <div class="content">
          <p id="title" class="title">${this.cardTitle}</p>
          <bbva-amount
            .amount="${this.amount}"
            .currencyCode="${this.currencyCode}"
            .localCurrency="${this.localCurrency}"
            .language="${this.language}"
            class="amount"
          ></bbva-amount>
          <p id="textAmount" class="text-amount" ?hidden="${!this.textAmount}">
            ${this.textAmount}
          </p>
          <p id="text" class="text ellipsis" ?hidden="${!this.text}">${this.text}</p>
          <bbva-badge-default
            id="badgeDefault"
            text="${this.badgeText}"
            class="${this.badgeType} badge"
            ?hidden="${!this.badgeText}"
          ></bbva-badge-default>
          <p id="date" class="date" ?hidden="${!this.date}">${this.date}</p>
        </div>
      </div>
    `;
  }

  _cardSelectedPress(e) {
    if (e.keyCode === KEY.SPACE) {
      this._changeSelected();
    }
  }

  /**
   * Apply focus itself.
   */
  focusElement() {
    this.shadowRoot.querySelector('#card').focus();
  }

  /**
   * Handler card selection
   * @event card-selected
   */

  _changeSelected() {
    this.selected = this.type === 'checkbox' ? !this.selected : true;
    this.dispatchEvent(
      new CustomEvent('card-select-change', {
        detail: this.selected,
        bubbles: true,
        composed: true,
      }),
    );
  }
}
customElements.define(BbvaCardSelect.is, BbvaCardSelect);
