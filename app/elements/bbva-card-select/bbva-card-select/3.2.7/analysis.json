{
  "schema_version": "1.0.0",
  "elements": [
    {
      "description": "Selectable card with title and amount.\n\nExample:\n\n```html\n<bbva-card-select\n  amount=\"15.20\"\n  badge-text=\"Activado\"\n  badge-type=\"warning\"\n  currency-code=\"EUR\"\n  date=\"Vence 09/12/2018\"\n  text-amount=\"año\"\n  text-info=\"Incluido\"\n  card-title=\"Modalidad Básica\">\n</bbva-card-select>\n```\n\n## Icons\n\nSince this component uses icons, it will need an [iconset](https://platform.bbva.com/en-us/developers/engines/cells/documentation/cells-architecture/composing-with-components/cells-icons) in your project as an application level dependency. In fact, this component uses an iconset in its demo.\n\n## Styling\n\nThe following custom properties are available for styling:\n\n### Custom properties\n\n| Selector                       | CSS Property      | CSS Variable                         | Theme Variable          | Foundations/Fallback                      |\n| ------------------------------ | ----------------- | ------------------------------------ | ----------------------- | ----------------------------------------- |\n| :host([selected]) .amount      | color             | --bbva-white                         |                         | #ffffff                                   |\n| :host([selected]) .badge       | color             | --bbva-white                         |                         | #ffffff                                   |\n| :host([selected]) .title       | color             | --bbva-card-select-color-selected    | --bbva-white            | #ffffff                                   |\n| :host([selected]) .card-select | background-color  | --bbva-card-select-bg-color-selected | --bbva-dark-medium-blue | #1464a5                                   |\n| :host([selected]) .card-select | color             | --bbva-card-select-color-selected    | --bbva-white            | #ffffff                                   |\n| .date                          | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .date                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .text                          | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .text                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .text-amount                   | margin-left       | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .title                         | margin-top        | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .title                         | margin-bottom     | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .title                         | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .title                         | color             | --bbva-medium-blue                   |                         | #1973b8                                   |\n| .info                          | margin-left       | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .info                          | line-height       | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .icon                          | transition        | --bbva-fast-transition               |                         | 0.2s                                      |\n| .icon                          | --cells-icon-size | --bbva-card-select-icon-size         | --bbva-global-spacer    | 0.5rem                                    |\n| .icon                          | color             | --bbva-white                         |                         | #ffffff                                   |\n| .icon-off                      | top               | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .icon-off                      | left              | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .icon-off                      | width             | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .icon-off                      | height            | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .icon-off                      | background-color  | --bbva-card-select-bg-color-icon-off | --bbva-400              | #bdbdbd                                   |\n| .card-select                   | width             | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .card-select                   | padding           | --bbva-global-spacer                 |                         | 0.5rem                                    |\n| .card-select                   | background-color  | --bbva-card-select-label-bg-color    | --bbva-white            | #fff                                      |\n| .card-select                   | transition        | --bbva-fast-transition               |                         | 0.2s                                      |\n| .card-select                   | box-shadow        |                                      | --boxShadowType1        | foundations.boxShadow.type1 |\n> Styling documentation generated by Cells CLI",
      "summary": "",
      "path": "bbva-card-select.js",
      "properties": [
        {
          "name": "_tabIndex",
          "type": "?",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 183,
              "column": 2
            },
            "end": {
              "line": 185,
              "column": 3
            }
          },
          "metadata": {
            "polymer": {
              "readOnly": true
            }
          }
        },
        {
          "name": "_ariaSelected",
          "type": "?",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 187,
              "column": 2
            },
            "end": {
              "line": 189,
              "column": 3
            }
          },
          "metadata": {
            "polymer": {
              "readOnly": true
            }
          }
        },
        {
          "name": "badgeText",
          "type": "string | null | undefined",
          "description": "Badge Text",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 88,
              "column": 6
            },
            "end": {
              "line": 91,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "badgeType",
          "type": "string | null | undefined",
          "description": "Badge Type",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 95,
              "column": 6
            },
            "end": {
              "line": 98,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "cardTitle",
          "type": "string | null | undefined",
          "description": "Card Title",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 102,
              "column": 6
            },
            "end": {
              "line": 105,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "date",
          "type": "string | null | undefined",
          "description": "Date text",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 109,
              "column": 6
            },
            "end": {
              "line": 111,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "text",
          "type": "string | null | undefined",
          "description": "Text",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 115,
              "column": 6
            },
            "end": {
              "line": 117,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "textAmount",
          "type": "string | null | undefined",
          "description": "Amount text",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 121,
              "column": 6
            },
            "end": {
              "line": 124,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "selected",
          "type": "boolean | null | undefined",
          "description": "selected",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 128,
              "column": 6
            },
            "end": {
              "line": 131,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "Boolean"
            }
          }
        },
        {
          "name": "textInfo",
          "type": "string | null | undefined",
          "description": "Information text",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 135,
              "column": 6
            },
            "end": {
              "line": 138,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "iconChecked",
          "type": "string | null | undefined",
          "description": "Icon",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 142,
              "column": 6
            },
            "end": {
              "line": 145,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "type",
          "type": "string | null | undefined",
          "description": "type checkbox or radio",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 149,
              "column": 6
            },
            "end": {
              "line": 151,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "String"
            }
          }
        },
        {
          "name": "iconCheckedSize",
          "type": "number | null | undefined",
          "description": "Icon size",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 155,
              "column": 6
            },
            "end": {
              "line": 158,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "Number"
            }
          }
        },
        {
          "name": "focusable",
          "type": "boolean | null | undefined",
          "description": "element focusable or not",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 162,
              "column": 6
            },
            "end": {
              "line": 164,
              "column": 7
            }
          },
          "metadata": {
            "polymer": {
              "attributeType": "Boolean"
            }
          }
        }
      ],
      "methods": [
        {
          "name": "updated",
          "description": "Fired after selected change",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 195,
              "column": 2
            },
            "end": {
              "line": 205,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "changedProps"
            }
          ],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "render",
          "description": "",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 215,
              "column": 2
            },
            "end": {
              "line": 264,
              "column": 3
            }
          },
          "metadata": {},
          "params": []
        },
        {
          "name": "_cardSelectedPress",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 266,
              "column": 2
            },
            "end": {
              "line": 270,
              "column": 3
            }
          },
          "metadata": {},
          "params": [
            {
              "name": "e"
            }
          ],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "focusElement",
          "description": "Apply focus itself.",
          "privacy": "public",
          "sourceRange": {
            "start": {
              "line": 275,
              "column": 2
            },
            "end": {
              "line": 277,
              "column": 3
            }
          },
          "metadata": {},
          "params": [],
          "return": {
            "type": "void"
          }
        },
        {
          "name": "_changeSelected",
          "description": "",
          "privacy": "protected",
          "sourceRange": {
            "start": {
              "line": 284,
              "column": 2
            },
            "end": {
              "line": 293,
              "column": 3
            }
          },
          "metadata": {},
          "params": [],
          "return": {
            "type": "void"
          }
        }
      ],
      "staticMethods": [],
      "demos": [
        {
          "url": "demo/index.html",
          "description": ""
        }
      ],
      "metadata": {},
      "sourceRange": {
        "start": {
          "line": 78,
          "column": 0
        },
        "end": {
          "line": 294,
          "column": 1
        }
      },
      "privacy": "public",
      "superclass": "HTMLElement",
      "name": "BbvaCardSelect",
      "attributes": [
        {
          "name": "badge-text",
          "description": "Badge Text",
          "sourceRange": {
            "start": {
              "line": 88,
              "column": 6
            },
            "end": {
              "line": 91,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "badge-type",
          "description": "Badge Type",
          "sourceRange": {
            "start": {
              "line": 95,
              "column": 6
            },
            "end": {
              "line": 98,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "card-title",
          "description": "Card Title",
          "sourceRange": {
            "start": {
              "line": 102,
              "column": 6
            },
            "end": {
              "line": 105,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "date",
          "description": "Date text",
          "sourceRange": {
            "start": {
              "line": 109,
              "column": 6
            },
            "end": {
              "line": 111,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "text",
          "description": "Text",
          "sourceRange": {
            "start": {
              "line": 115,
              "column": 6
            },
            "end": {
              "line": 117,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "text-amount",
          "description": "Amount text",
          "sourceRange": {
            "start": {
              "line": 121,
              "column": 6
            },
            "end": {
              "line": 124,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "selected",
          "description": "selected",
          "sourceRange": {
            "start": {
              "line": 128,
              "column": 6
            },
            "end": {
              "line": 131,
              "column": 7
            }
          },
          "metadata": {},
          "type": "boolean | null | undefined"
        },
        {
          "name": "text-info",
          "description": "Information text",
          "sourceRange": {
            "start": {
              "line": 135,
              "column": 6
            },
            "end": {
              "line": 138,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "icon-checked",
          "description": "Icon",
          "sourceRange": {
            "start": {
              "line": 142,
              "column": 6
            },
            "end": {
              "line": 145,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "type",
          "description": "type checkbox or radio",
          "sourceRange": {
            "start": {
              "line": 149,
              "column": 6
            },
            "end": {
              "line": 151,
              "column": 7
            }
          },
          "metadata": {},
          "type": "string | null | undefined"
        },
        {
          "name": "icon-checked-size",
          "description": "Icon size",
          "sourceRange": {
            "start": {
              "line": 155,
              "column": 6
            },
            "end": {
              "line": 158,
              "column": 7
            }
          },
          "metadata": {},
          "type": "number | null | undefined"
        },
        {
          "name": "focusable",
          "description": "element focusable or not",
          "sourceRange": {
            "start": {
              "line": 162,
              "column": 6
            },
            "end": {
              "line": 164,
              "column": 7
            }
          },
          "metadata": {},
          "type": "boolean | null | undefined"
        }
      ],
      "events": [
        {
          "type": "CustomEvent",
          "name": "card-select-change",
          "description": "Fired after selected change",
          "metadata": {}
        },
        {
          "type": "CustomEvent",
          "name": "card-selected",
          "description": "Handler card selection",
          "metadata": {}
        }
      ],
      "styling": {
        "cssVariables": [],
        "selectors": []
      },
      "slots": [],
      "tagname": "bbva-card-select",
      "mixins": [
        "CellsAmountMixin"
      ]
    }
  ]
}
