# `bbva-card-select`

## `Semantic Dom`

####   `DOM - Structure test`

```html
<bbva-card-select>
</bbva-card-select>

```

####   `SHADOW DOM - Structure test`

```html
<div
  aria-checked="false"
  class="card-select"
  id="card"
  role="checkbox"
  tabindex="0"
>
  <div class="header">
    <span
      class="icon-off"
      style=""
    >
    </span>
    <cells-icon
      class="icon opacity-icon"
      icon="coronita:correct"
      id="icon"
    >
    </cells-icon>
    <span
      class="info"
      id="textInfo"
    >
    </span>
  </div>
  <div class="content">
    <p
      class="title"
      id="title"
    >
    </p>
    <bbva-amount
      class="amount"
      variant="3xl"
    >
    </bbva-amount>
    <p
      class="text-amount"
      hidden=""
      id="textAmount"
    >
    </p>
    <p
      class="ellipsis text"
      hidden=""
      id="text"
    >
    </p>
    <bbva-badge-default
      class="badge"
      hidden=""
      id="badgeDefault"
      text=""
    >
    </bbva-badge-default>
    <p
      class="date"
      hidden=""
      id="date"
    >
    </p>
  </div>
</div>

```

####   `LIGHT DOM - Structure test`

```html

```

