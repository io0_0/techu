/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

var express = require("express");
var bodyParser = require("body-parser");
var awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");

const requestJson = require("request-json");
const oracledb = require("oracledb");
const moment = require("moment");
const URL_BASE = "/account/";
const URL_MLAB = "https://api.mlab.com/api/1/databases/techu5db/collections/";
const API_KEY = "apiKey=" + "NQCR6_EMDAdqyM6VEWg3scF_k32uwvHF";

// declare a new express app
var app = express();

app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

console.log("hola amigo");
/**********************
 * Example get method *
 **********************/
app.get("/holamundos", function(req, res) {
  console.log("hola mundo");
  res.send("hola eduardo cuellar"); //la respuseta se envia siempre en ultimo lugar porque se corta la comunicacion
});

app.put(URL_BASE + "accounts/:id", function(req, resp) {
  let aorigen = req.body.account;
  let queryString = `q={"account":"${aorigen}"}&`;
  console.log(aorigen);
  let fieldString = 'f={"_id":0}&';
  let httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get("accounts?" + queryString + fieldString + API_KEY, function(
    error,
    respuestaMLab,
    body
  ) {
    body[0].balance = req.body.balance;
    var cambio = '{"$set":' + JSON.stringify(body[0]) + "}";
    console.log(cambio);
    console.log(body);

    httpClient.put(
      URL_MLAB + `accounts?q={"account": "${aorigen}"}&` + API_KEY,
      JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:" + body);
        resp.status(201).send(body);
      }
    );
  });
});

// tabla transacciones
app.post(URL_BASE + "movimientos", (request, response) => {
  let aorigen = request.body.account_origen;
  console.log(aorigen);
  let queryString = `q={"account_origen":"${aorigen}"}&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0}&';
  let httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get(
    "transactions?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          res = {};
        }
      }
      let respuesta = res;
      console.log(respuesta);

      queryString = `q={"account_destino":"${aorigen}"}&`;
      //let fieldString = 'f={"_id":0,"account":1}&';
      httpClient.get(
        "transactions?" + queryString + fieldString + API_KEY,
        (error, resMlab, body) => {
          if (error) {
            res = {
              msg: "Error en la peticion a mLab"
            };
            response.status(500);
          } else {
            if (body.length > 0) {
              res = body;
            } else {
              res = {};
            }
          }
          console.log(res);
          console.log(respuesta);
          if (respuesta.length > 0 && res.length > 0) {
            let respuesta2 = respuesta.concat(res);

            response.send(respuesta2);
          } else {
            if (respuesta.length > 0) {
              response.send(respuesta);
              return;
            }
            if (res.length > 0) {
              response.send(res);
              return;
            }
            response.status(204);

            res = {
              msg: "movimientos no encontrados"
            };
            response.send(res);
          }
        }
      );
    }
  );
});
app.post(URL_BASE + "accounts", function(request, response) {
  let account = request.body.account;
  let currencyCode = request.body.currency_code;
  let balance = request.body.balance;
  let idUsuario = request.body.id_user;
  let today = moment(new Date()).format("YYYY-MM-DD[T00:00:00.000Z]");
  console.log("POST origen = " + account + "  importe = " + balance);
  //let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
  let count_param = `user_account?${(c = true)}&`;
  let newtransacciton = {
    account: account,
    currency_code: currencyCode,
    balance: balance,
    id_user: idUsuario
  };
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.post(
    "accounts?" + API_KEY,
    newtransacciton,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        res = {
          msg: "transferecia ejectuada"
        };
        response.status(201);
      }
      response.send(res);
    }
  );
});

app.post(URL_BASE + "transfer", function(request, response) {
  let aorigen = request.body.account_origen;
  let adestino = request.body.account_destino;
  let importe = request.body.importe;
  let tcambio = request.body.tipo_cambio;
  let today = moment(new Date()).format("YYYY-MM-DD[T00:00:00.000Z]");
  console.log("POST origen = " + aorigen + "  importe = " + importe);
  //let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
  let count_param = `user_account?${(c = true)}&`;
  let newtransacciton = {
    account_origen: aorigen,
    importe_origen: importe,
    account_destino: adestino,
    importe_destino: importe,
    tipo_cambio: tcambio,
    fecha_mov: today
  };
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.post(
    "transactions?" + API_KEY,
    newtransacciton,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        res = {
          msg: "transferecia ejectuada"
        };
        response.status(201);
      }
      response.send(res);
    }
  );
});

//tabla account
app.get(URL_BASE + "users/:id/accounts", (request, response) => {
  console.log(request.params.id);
  let id_user = request.params.id;
  let queryString = `q={"id_user":${id_user}}&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0,"id_user":0}&';
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get(
    "accounts?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = {
            msg: "Cuentas no encontradas"
          };
        }
      }
      response.send(res);
    }
  );
});




app.get(URL_BASE + "accounts/:id", (request, response) => {
  console.log(request.params.id);
  let id_account = request.params.id;
  console.log("acount from query", id_account);
  let queryString = `q={"account":'${id_account}' }&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0}&';
  const httpClient = requestJson.createClient(URL_MLAB);
  let query = URL_MLAB + "accounts?" + queryString + fieldString + API_KEY;
  console.log(query);
  httpClient.get(
    "accounts?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = {
            msg: "Cuentas no encontradas"
          };
        }
      }
      console.log(res);

      response.send(res);
    }
  );
});



app.get(URL_BASE + "accounts/:id/user/", (request, response) => {
  console.log(request.params.id);
  let id_account = request.params.id;
  console.log("acount from query", id_account);
  let queryString = `q={"account":'${id_account}' }&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0}&';
  const httpClient = requestJson.createClient(URL_MLAB);
  let query = URL_MLAB + "accounts?" + queryString + fieldString + API_KEY;
  console.log(query);
  httpClient.get(
    "accounts?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      let res = [];
      let bandera = false;

      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
          bandera = true;
        } else {
          response.status(404);
          res = {
            msg: "Cuentas del user no encontradas"
          };
          response.send(res);
        }
      }
      console.log(bandera );
      console.log(res);
      if (bandera ) {
        console.log(bandera );

        let accountUserId = res[0].id_user;

        console.log(accountUserId);
        queryString = `q={"id_user":${accountUserId}}&`;
        //let fieldString = 'f={"_id":0,"account":1}&';
        fieldString = 'f={"_id":0}&';
        res = {};
        httpClient.get(
          "user?" + queryString + fieldString + API_KEY,
          (error, resMlab, body) => {
            res = {};
            if (error) {
              res = {
                msg: "Error en la peticion a mLab"
              };
              response.status(500);
            } else {
              if (body.length > 0) {
                res = body;
              } else {
                response.status(404);
                res = {
                  msg: "usuario  no encontradas"
                };
              }
            }

            response.send(res);
          }
        );
      }
    }
  );
});

/// tabla user
app.get(URL_BASE + "users/:id", (request, response) => {
  console.log(request.params.id);
  let id_user = request.params.id;
  let queryString = `q={"id_user":${id_user}}&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0,"id_user":0}&';
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get(
    "user?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(204);
          res = {
            msg: "Cuentas del id no encontradas"
          };
        }
      }
      console.log(res);

      response.send(res);
    }
  );
});


app.post(URL_BASE + "email/accounts", (request, response) => {
  console.log( request.body.email);
  let email = request.body.email;
  let queryString = `q={"email":'${email}'}&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0}&';
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get(
    "user?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(204);
          res = {
            msg: "Cuentas del id no encontradas"
          };
        }
      }
      console.log(res);

      let id_user =res[0].id_user
       queryString = `q={"id_user":${id_user}}&`;
      //let fieldString = 'f={"_id":0,"account":1}&';
      fieldString = 'f={"_id":0,"id_user":0}&';
      const httpClient = requestJson.createClient(URL_MLAB);
      httpClient.get(
        "accounts?" + queryString + fieldString + API_KEY,
        (error, resMlab, body) => {
          let res = {};
          if (error) {
            res = {
              msg: "Error en la peticion a mLab"
            };
            response.status(500);
          } else {
            if (body.length > 0) {
              res = body;
            } else {
              response.status(404);
              res = {
                msg: "Cuentas no encontradas"
              };
            }
          }
          response.send(res);
        }
      );
    }
  );
});



app.get(URL_BASE + "users/dni/:dni", (request, response) => {
  console.log(request.params.dni);
  let dni = request.params.dni;
  let queryString = `q={"dni":${dni}}&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0,"dni":0}&';
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get(
    "user?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = {
            msg: "Cuentas no encontradas"
          };
        }
      }
      response.send(res);
    }
  );
});

app.get(URL_BASE + "users/cel/:cel", (request, response) => {
  console.log(request.params.dni);
  let cel = request.params.cel;
  let queryString = `q={"nrocel":${cel}}&`;
  //let fieldString = 'f={"_id":0,"account":1}&';
  let fieldString = 'f={"_id":0,"id_user":1}&';
  const httpClient = requestJson.createClient(URL_MLAB);
  httpClient.get(
    "user?" + queryString + fieldString + API_KEY,
    (error, resMlab, body) => {
      let res = {};
      if (error) {
        res = {
          msg: "Error en la peticion a mLab"
        };
        response.status(500);
      } else {
        if (body.length > 0) {
          res = body;
        } else {
          response.status(404);
          res = {
            msg: "Cuentas no encontradas"
          };
        }
      }
      response.send(res);
    }
  );
});
//Method POST login
app.post(URL_BASE + "login", function(req, res) {
  let email = req.body.email;
  let pass = req.body.password;
  console.log("POST email = " + email + "  pass = " + pass);
  let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
  let limFilter = "l=1&";
  let clienteMlab = requestJson.createClient(URL_MLAB);
  clienteMlab.get("user?" + queryString + limFilter + API_KEY, function(
    error,
    respuestaMLab,
    body
  ) {
    if (!error) {
      if (body.length == 1) {
        // Existe un usuario que cumple 'queryString'
        let login = '{"$set":{"logged":true}}';
        clienteMlab.put(
          'user?q={"id_user": ' + body[0].id_user + "}&" + API_KEY,
          JSON.parse(login),
          //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
          function(errPut, resPut, bodyPut) {
            res.send({
              msg: "Login correcto",
              user: body[0].email,
              userid: body[0].id_user,
              name: body[0].first_name
            });
            // If bodyPut.n == 1, put de mLab correcto
          }
        );
      } else {
        res.status(404).send({
          msg: "Usuario no válido."
        });
      }
    } else {
      res.status(500).send({
        msg: "Error en petición a mLab."
      });
    }
  });
});

//Method POST logout
app.post(URL_BASE + "logout", function(req, res) {
  var email = req.body.email;
  console.log("POST email = " + email);
  var queryString = 'q={"email":"' + email + '","logged":true}&';
  console.log(queryString);
  var clienteMlab = requestJson.createClient(URL_MLAB);
  clienteMlab.get("user?" + queryString + API_KEY, function(
    error,
    respuestaMLab,
    body
  ) {
    var respuesta = body[0]; // Asegurar único usuario
    if (!error) {
      if (respuesta != undefined) {
        // Existe un usuario que cumple 'queryString'
        let logout = '{"$unset":{"logged":true}}';
        clienteMlab.put(
          'user?q={"id_user": ' + respuesta.id_user + "}&" + API_KEY,
          JSON.parse(logout),
          //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
          function(errPut, resPut, bodyPut) {
            res.send({
              msg: "Logout correcto",
              user: respuesta.email
            });
            // If bodyPut.n == 1, put de mLab correcto
          }
        );
      } else {
        res.status(404).send({
          msg: "Logout failed!"
        });
      }
    } else {
      res.status(500).send({
        msg: "Error en petición a mLab."
      });
    }
  });
});

app.listen(3000, function() {
  console.log("App started");
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file

app._router.stack.forEach(function(r) {
  if (r.route && r.route.path) {
    console.log(r.route.path);
  }
});
module.exports = app;
