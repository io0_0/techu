# API REST Proyecto Pratitioner

Este API REST está desplegado en  Lambda :  (https://k1oire3wnb.execute-api.us-east-1.amazonaws.com/dev/)

Este API REST esta diseñado para la proyecto final del programa BBVA Tech Universit.

Todo el codigo esta alojado en Bitbucket en el siguiente repositorio:  (https://bitbucket.org/io0_0/techu/src/master/)

Se uso el gestor Amplify para el despliegue de las funciones lambda

## Introducción
Este API esta desarrollado para un una aplicación de banca móvil (webapp) con una funcionalidad básica.

Para todos los metodos de POST y PUT tendremos que pasarle un JSON con los campos necesarios, y para metodos POST, PUT y DELETE será necesario una autentificacion. Otros metodos como los GET de un solo objeto estan implementados con hipermedia enlazando con los objetos que tengas relacionados.

## Casos de Uso
### GET:
- **Usuarios**

	- **apitechu/v2/login** -> Busca un usuario por login, muestra los ID* de los comentarios realizados por el usuario que se busca. En este caso de uso hemos implementado hipermedia relacionando todos los comentarios que ha hecho el usuario.
	- **apitechu/v2/users/:id** -> Consulta de datos del cliente por id.
	- **apitechu/v2/users/dni/:dni** -> Consulta de datos del cliente por dni.
	- **apitechu/v2/users/cel/:cel** -> Consulta de datos del cliente por Celular.


- **Cuentas**

	-   **apitechu/v2/users/:id/accounts** -> Devuelve las cuentas asociadas a un cliente.
    -   **apitechu/v2/accounts/:id** -> Devuelve la informacion de un numero de cuenta.  
    -   **apitechu/v2/accounts/:id/user** -> Devuelve la informacion del cliente  cuenta una cuenta.


### POST
- **Usuarios**

	- **/apitechu/v2/login** -> Busca un usuario por email y contraseña, Si se encuentra el documento se le añade un parametro " {"logged":true} " . En caso no se encuentre mandará un error.
	- **/apitechu/v2/logout** -> Busca un usuario por email y el parametro {"logged":true}, en caso no se encuentre mandará mensaje de error.
	- **/apitechu/v2/new user** -> A partir de un JSON que se le pasa añade un nuevo usuario a la base de datos.


- **Cuentas**
	- **/apitechu/v2/transfer** -> Hace una transferencian entre cuentas de clientes, para esto necesarias la cuenta de origen , destino , importe y tipo de cambio (solo si es bimoneda).
	- **/apitechu/v2/movimientos** -> Muestra los movimientos de las cuentas de un clientes, para esto se envia la cuenta
	- **/apitechu/v2/accounts** -> Crea una nueva cuena
	- **apitechu/v2/email/accounts** -> Devuelve las cuentas asociadas a un correo.

###PUT
- **Cuentas**

	- **/apitechu/v2/accounts/:id** -> Actualiza la informacion de una cuenta




## Lista de comandos para puesta en marcha
#### Para poner en marcha la aplicación tendremos que realizar los siguientes comandos:

 `npm install	 //con esto instalaremos los modulos necesarios para node.js`

> Este paso lo ejecutaremos siempre que trabajemos con la Base de Datos en local y no con una en remoto, en este caso también tendremos que cambiar la conexion de la BBDD poniendola con la direccion local.


Si solo vamos a probar la aplicacion es recomendable este:

`node sever.js`

Si aparte de probar vamos a realizar cambios y no queremos estar ejecutando el servidor cada vez que hagamos un cambio usaremos este otro comando:

`nodemon server.js`

##### Para realizar las pruebas usaremos una aplicacion llamada PostMan, esta aplicacion permite realizar las peticiones necesarias para un API REST (GET, POST, DELETE, PUT).

## Requisitos "Adicionales" implementados
A parte de los requisitos minimos exigidos para superar la práctica se han implementado los siguientes requisitos adicionales que se ofrecían:  

- Implementar una Base de Datos no relacional, en nuestro MongoDB, esta base de datos es remota y se usa por medio de mLab.

- Desplegar el API en algun servicio en la nube, se ha uso Lambda
- Desplegar un webhook para en google Cloud function  que consume algunos de los servios seleccionados
-

La pagina esta desplegada en   (https://k1oire3wnb.execute-api.us-east-1.amazonaws.com/dev/)
